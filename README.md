# Croatiae Auctores Latini (CroaLa) – a Neo-Latin Corpus for Fun and Profit

[Neven Jovanović](orcid.org/0000-0002-9119-399X), Department of Classical Philology, Faculty of Humanities and Social Sciences, University of Zagreb

Presentation address: [croala.ffzg.unizg.hr/croala-florilegia](http://croala.ffzg.unizg.hr/croala-florilegia)

![Florilegia on Pexels](img/vintage-music-business-shop.jpg)

## What?

A presentation for the Global Philology [Florilegia: Big Textual Data Workshop](http://www.dh.uni-leipzig.de/wo/events/global-philology-big-textual-data/), July 10-11, 2017.

# Summary

Croatiae auctores Latini (CroALa) is a digital collection of texts written by authors connected with Croatia, from 10th to 20th century. First published in 2009, the collection at the moment contains over 450 documents with over 5 million words, encoded as TEI XML. It also exists in several forms, accessible at several locations. Locations, integrations, and future plans will be presented.


## How?

The slides for the presentation are made with the [reveal.js](https://github.com/hakimel/reveal.js/) HTML presentation framework. The treemap chart is made with Mike Bostock's [d3](https://bl.ocks.org/mbostock/4063582).

# Licence

[CC-BY](LICENSE.md)
